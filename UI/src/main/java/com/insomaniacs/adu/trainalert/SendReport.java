package com.insomaniacs.adu.trainalert;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SendReport extends AppCompatActivity {
    public void onBackPressed(){
        startActivity(new Intent(SendReport.this, HomeNav.class));
    }

    final String UID = "UID";
    final String REPORT_NAME = "REPORT_NAME";
    final String TRAIN_NUMBER = "TRAIN_NUMBER";
    final String ACTIVE = "ACTIVE";
    final String YES = "YES";
    final String NO = "NO";
    final String TIME_CREATED = "TIME_CREATED";

    ImageView doorOpened;
    ImageView malfunctioningTrain;
    ImageView trainStopped;
    ImageView withoutAircon;
    ImageView cutTrains;

    String Uid;
    String ReportName;
    String TrainNumber;
    String Active;
    String Yes;
    String No;
    String TimeCreated;
    FirebaseUser user;
    String uid;

    TextView display;
    String record="";
    Spinner sp;
    String names []= {"Train 1", "Train 2", "Train 3"};
    ArrayAdapter <String> adapter;

    Calendar currnetDateTime ;
    SimpleDateFormat df = new SimpleDateFormat("HH:mm a");
    String currentTime;
    Spinner trainSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_report);

        user = FirebaseAuth.getInstance().getCurrentUser();
        uid = user.getUid();

//        FirebaseAuth auth = FirebaseAuth.getInstance();
//        FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
//                if (firebaseUser != null) {
//                    userId = firebaseUser.getUid();
//                }
//            }
//        };

        doorOpened = (ImageView) findViewById(R.id.dooropened);
        malfunctioningTrain = (ImageView) findViewById(R.id.malfunctioningtrain);
        trainStopped = (ImageView) findViewById(R.id.trainstopped);
        withoutAircon = (ImageView) findViewById(R.id.withoutac);
        cutTrains = (ImageView) findViewById(R.id.cuttrains);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.child("Locations").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<String> trainNums = new ArrayList<String>();

                for (DataSnapshot trainSnapshot: dataSnapshot.getChildren()) {
                    trainNums.add(trainSnapshot.getKey());
                    //Toast.makeText(SendReport.this, "Key: " + trainNums, Toast.LENGTH_SHORT).show();
                }

                trainSpinner = (Spinner) findViewById(R.id.spinner1);
                ArrayAdapter<String> trainsAdapter = new ArrayAdapter<String>(SendReport.this, android.R.layout.simple_spinner_item, trainNums);
                trainsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                trainSpinner.setAdapter(trainsAdapter);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        /*
        sp = (Spinner) findViewById(R.id.spinner);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        display=(TextView) findViewById(R.id.chosen_train_number);
        sp.setAdapter(adapter);


       sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               switch (position) {
                   case 0:
                   record = "#1";
                       display.setTextSize(20);
                       display.setText(record);
                   break;

                   case 1:
                       record = "#2";
                       display.setTextSize(20);
                       display.setText(record);
                       break;
                   case 2:
                       record = "#3";
                       display.setTextSize(20);
                       display.setText(record);
                       break;
               }
           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });
       */
        doorOpened.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = instantiateFirebase("Reports");
                currnetDateTime = Calendar.getInstance();;
                currentTime = df.format(currnetDateTime.getTime());
                Uid = uid;
                ReportName = "Door Opened";
                TrainNumber = trainSpinner.getSelectedItem().toString();
                Active = "0";
                Yes = "0";
                No = "0";
                TimeCreated = currentTime;

                Map<String, String> reportRecord = new HashMap<>();
                reportRecord.put(UID, Uid);
                reportRecord.put(REPORT_NAME, ReportName);
                reportRecord.put(TRAIN_NUMBER, TrainNumber);
                reportRecord.put(ACTIVE, Active);
                reportRecord.put(YES, Yes);
                reportRecord.put(NO, No);
                reportRecord.put(TIME_CREATED, TimeCreated);

                addReport(ref, reportRecord);

            }
        });

        malfunctioningTrain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = instantiateFirebase("Reports");
                currnetDateTime = Calendar.getInstance();;
                currentTime = df.format(currnetDateTime.getTime());
                Uid = uid;
                ReportName = "Malfunctioning Train";
                TrainNumber = trainSpinner.getSelectedItem().toString();
                Active = "0";
                Yes = "0";
                No = "0";
                TimeCreated = currentTime;

                Map<String, String> reportRecord = new HashMap<>();
                reportRecord.put(UID, Uid);
                reportRecord.put(REPORT_NAME, ReportName);
                reportRecord.put(TRAIN_NUMBER, TrainNumber);
                reportRecord.put(ACTIVE, Active);
                reportRecord.put(YES, Yes);
                reportRecord.put(NO, No);
                reportRecord.put(TIME_CREATED, TimeCreated);

                addReport(ref, reportRecord);

            }
        });

        trainStopped.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = instantiateFirebase("Reports");
                currnetDateTime = Calendar.getInstance();;
                currentTime = df.format(currnetDateTime.getTime());
                Uid = uid;
                ReportName = "Train Stopped";
                TrainNumber = trainSpinner.getSelectedItem().toString();
                Active = "0";
                Yes = "0";
                No = "0";
                TimeCreated = currentTime;

                Map<String, String> reportRecord = new HashMap<>();
                reportRecord.put(UID, Uid);
                reportRecord.put(REPORT_NAME, ReportName);
                reportRecord.put(TRAIN_NUMBER, TrainNumber);
                reportRecord.put(ACTIVE, Active);
                reportRecord.put(YES, Yes);
                reportRecord.put(NO, No);
                reportRecord.put(TIME_CREATED, TimeCreated);

                addReport(ref, reportRecord);

            }
        });

        withoutAircon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = instantiateFirebase("Reports");
                currnetDateTime = Calendar.getInstance();;
                currentTime = df.format(currnetDateTime.getTime());
                Uid = uid;
                ReportName = "Without Aircon";
                TrainNumber = trainSpinner.getSelectedItem().toString();
                Active = "0";
                Yes = "0";
                No = "0";
                TimeCreated = currentTime;

                Map<String, String> reportRecord = new HashMap<>();
                reportRecord.put(UID, Uid);
                reportRecord.put(REPORT_NAME, ReportName);
                reportRecord.put(TRAIN_NUMBER, TrainNumber);
                reportRecord.put(ACTIVE, Active);
                reportRecord.put(YES, Yes);
                reportRecord.put(NO, No);
                reportRecord.put(TIME_CREATED, TimeCreated);

                addReport(ref, reportRecord);

            }
        });

        cutTrains.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference ref = instantiateFirebase("Reports");
                currnetDateTime = Calendar.getInstance();;
                currentTime = df.format(currnetDateTime.getTime());
                Uid = uid;
                ReportName = "Cut Trains";
                TrainNumber = trainSpinner.getSelectedItem().toString();
                Active = "0";
                Yes = "0";
                No = "0";
                TimeCreated = currentTime;

                Map<String, String> reportRecord = new HashMap<>();
                reportRecord.put(UID, Uid);
                reportRecord.put(REPORT_NAME, ReportName);
                reportRecord.put(TRAIN_NUMBER, TrainNumber);
                reportRecord.put(ACTIVE, Active);
                reportRecord.put(YES, Yes);
                reportRecord.put(NO, No);
                reportRecord.put(TIME_CREATED, TimeCreated);

                addReport(ref, reportRecord);

            }
        });
    }

    public DatabaseReference instantiateFirebase(String parent) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(parent);
        return myRef;
    }


    public static class Report {
        public String ReportName;
        public String TrainNumber;
        public String Active;
        public String Yes;
        public String No;
        public String TimeCreated;

        public Report (String reportname, String trainnumber,String active, String yes, String no, String timecreated) {
            this.ReportName = reportname;
            this.TrainNumber = trainnumber;
            this.Active = active;
            this.Yes = yes;
            this.No = no;
            this.TimeCreated = timecreated;
        }
    }

    public void addReport(DatabaseReference ref, Map map) {
        final String uid = (String) map.get(UID);
        final String reportName = (String) map.get(REPORT_NAME);
        final String trainNumber = (String) map.get(TRAIN_NUMBER);
        final String active = (String) map.get(ACTIVE);
        final String yes = (String) map.get(YES);
        final String no = (String) map.get(NO);
        final String timeCreated = (String) map.get(TIME_CREATED);

        Report report = new Report(reportName ,trainNumber ,active, yes, no, timeCreated);

        ref.child(uid).setValue(report, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Toast.makeText(SendReport.this, "Success!", Toast.LENGTH_SHORT).show();
            }
        });
       }
    }




