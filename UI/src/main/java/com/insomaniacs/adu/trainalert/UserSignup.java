package com.insomaniacs.adu.trainalert;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class UserSignup extends AppCompatActivity {
    public void onBackPressed(){
        finish();
        startActivity(new Intent(UserSignup.this, UserLogin.class));
    }

    private Button signinbutton;
    private EditText  editTextEmail;
    private EditText editTextPassword;
    private ProgressDialog progressdialog;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_signup);

        progressdialog= new ProgressDialog(this);
        firebaseAuth= FirebaseAuth.getInstance();
        signinbutton= (Button) findViewById(R.id.SignInButton);
        editTextEmail= (EditText) findViewById(R.id.EmailEditText);
        editTextPassword= (EditText) findViewById(R.id.PasswordEditText);

//        if (firebaseAuth.getCurrentUser() !=null){
//            finish();
//            startActivity(new Intent(getApplicationContext(), UserLogin.class));
//        }
    }


    private void SignUpAuth() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter Email address", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }

        progressdialog = ProgressDialog.show(UserSignup.this, "", "Authenticating...");
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    finish();
                    startActivity(new Intent(getApplicationContext(), HomeNav.class));
                }

                else {
                    progressdialog.hide();
                    Toast.makeText(UserSignup.this, "Sign Up failed. Try Again", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void SigninClick (View v){
        SignUpAuth();
        //        startActivity(new Intent(UserSignup.this, HomeNav.class));
    }
}