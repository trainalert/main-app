package com.insomaniacs.adu.trainalert;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewReports extends Fragment {

    List<String> mTrainRecords = new ArrayList<>();
    ArrayAdapter<String> adapter;

    ListView mRecordsListView;


    final String UID = "UID";
    final String REPORT_NAME = "REPORT_NAME";
    final String TRAIN_NUMBER = "TRAIN_NUMBER";

    String UIDofReporter;
    String storeUID;
    String reportName;
    String trainNumber;
    String no;
    String yes;
    String yesToString, noToString;
    Integer yesToInt, noToInt;
    boolean voted = false;

    RadioButton radYes;
    RadioButton radNo;

    public ViewReports() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_viewreports, container, false);

        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, mTrainRecords);

        DatabaseReference ref = instantiateFirebase("Reports");


        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mTrainRecords.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    UIDofReporter = (String) postSnapshot.getKey();
                    trainNumber = (String) ((Map) postSnapshot.getValue()).get("TrainNumber");
                    reportName = (String) ((Map) postSnapshot.getValue()).get("ReportName");
                    no = (String) ((Map) postSnapshot.getValue()).get("Yes");
                    yes = (String) ((Map) postSnapshot.getValue()).get("No");

                    mTrainRecords.add(String.format(Locale.ENGLISH, "%s : Train %s: %s Yes = %s, No = %s", UIDofReporter ,trainNumber, reportName, no, yes));
                }

                //Toast.makeText(getActivity(), "Yes is: " + yes, Toast.LENGTH_SHORT).show();

                //mapToList((Map)dataSnapshot.getValue());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final DatabaseReference ref = instantiateFirebase("Reports");

        mRecordsListView = (ListView) getView().findViewById(R.id.recordsListView);
        radYes = (RadioButton) getView().findViewById(R.id.rBtnYes);
        radNo = (RadioButton) getView().findViewById(R.id.rBtnNo);

        radYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radYes.isChecked()) {
                    radNo.setChecked(false);
                    mRecordsListView.setEnabled(true);
                }
            }
        });
        radNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radNo.isChecked()) {
                    radYes.setChecked(false);
                    mRecordsListView.setEnabled(true);
                }
            }
        });


        mRecordsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, View view, final int position, long id) {
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (radYes.isChecked()) {
                                    String hisUID = (String) mRecordsListView.getItemAtPosition(position).toString();
                                    //Separates the string to only get the UID
                                     String userID = hisUID.substring(0, hisUID.indexOf(" "));

                                    if (voted != true) {
                                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                            String compareme = postSnapshot.getKey();
                                            if (compareme.trim().equals(userID.trim())) {
                                                storeUID = postSnapshot.getKey();
                                                yesToString = (String) ((Map) postSnapshot.getValue()).get("Yes");
                                                yesToInt = Integer.valueOf(yesToString);
                                                yesToInt += 1;
                                                yesToString = String.valueOf(yesToInt);
                                                ref.child(storeUID).child("Yes").setValue(yesToString);
                                                noToString = (String) ((Map) postSnapshot.getValue()).get("No");
                                                noToInt = Integer.valueOf(noToString);

                                                if (yesToInt > noToInt && yesToInt - noToInt == 5) {
                                                    DatabaseReference admin = instantiateFirebase("AdminReport");
                                                    Map<String, String> adminRecord = new HashMap<>();
                                                    adminRecord.put(UID, storeUID);
                                                    adminRecord.put(REPORT_NAME, reportName);
                                                    adminRecord.put(TRAIN_NUMBER, trainNumber);
                                                    addAdmin(admin, adminRecord);
                                                    ref.child(storeUID).removeValue();
                                                    adapter.notifyDataSetChanged();
                                                    radYes.setChecked(false);
                                                }
                                                voted = true;
                                            }
                                        }
                                    }

                        } else if (radNo.isChecked()) {
                            String hisUID = (String) mRecordsListView.getItemAtPosition(position).toString();
                            //Separates the string to only get the UID
                            String userID = hisUID.substring(0, hisUID.indexOf(" "));

                            if (voted != true) {
                                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                    String compareme = postSnapshot.getKey();
                                    if (compareme.trim().equals(userID.trim())) {
                                        storeUID = postSnapshot.getKey();
                                        noToString = (String) ((Map) postSnapshot.getValue()).get("No");
                                        noToInt = Integer.valueOf(noToString);
                                        noToInt += 1;
                                        noToString = String.valueOf(noToInt);
                                        ref.child(storeUID).child("No").setValue(noToString);
                                        yesToString = (String) ((Map) postSnapshot.getValue()).get("Yes");
                                        yesToInt = Integer.valueOf(yesToString);

                                        if (noToInt > yesToInt && noToInt - yesToInt == 5) {
                                            ref.child(storeUID).removeValue();
                                            radNo.setChecked(false);
                                            adapter.notifyDataSetChanged();
                                        }
                                        voted = true;
                                    }
                                }
                            }
                        }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                voted = false;
            }
        });

        mRecordsListView.setAdapter(adapter);

    }

    public DatabaseReference instantiateFirebase(String parent) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(parent);
        return myRef;
    }


    public static class Admin {
        public String ReportName;
        public String TrainNumber;


        public Admin (String reportname, String trainnumber) {
            this.ReportName = reportname;
            this.TrainNumber = trainnumber;
        }
    }
    public void addAdmin(DatabaseReference ref, Map map) {
        final String uid = (String) map.get(UID);
        final String reportName = (String) map.get(REPORT_NAME);
        final String trainNumber = (String) map.get(TRAIN_NUMBER);

        Admin admin = new Admin(reportName ,trainNumber);

        ref.child(uid).setValue(admin, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Toast.makeText(getActivity(), "Success!", Toast.LENGTH_SHORT).show();
            }
        });

    }
}

