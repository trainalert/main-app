package com.insomaniacs.adu.trainalert;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DriverScreen extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public void onBackPressed() {
//        startActivity(new Intent(DriverScreen.this, HomeNav.class));
        moveTaskToBack(true);
    }

    private static final String TAG = "CurrentLocationApp";
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    int convTime, convOldTime, timeInMins = 0, oldTimeInMins;
    double lat, lng, distance, distance1, distance2, spd, oldtime, time;
    double oldlat = 0.0;
    double oldlng = 0.0;
    String Latitude;
    String Longitude;
    String Bound;
    String StationName;
    public  Location pushLocation = new Location("");
    int stationNum = 0; // used to check if train has passed all stations
    List<LatLng> points = new ArrayList<LatLng>();
    ArrayList<LatLng> northBound = new ArrayList<LatLng>();
    ArrayList<LatLng> southBound = new ArrayList<LatLng>();
    // This area is used for firebase //
    final String TRAIN_NUMBER = "TRAIN_NUMBER";
    final String LATITUDE = "LATITUDE";
    final String LONGITUDE = "LONGITUDE";
    final String BOUND = "BOUND";
    String TrainNum;
    boolean clicked = false;
    private TextView mLatitudeText;
    private TextView mLongitudeText;
    private EditText editTxtTrainNum;
    private RadioButton rBtnNorth, rBtnSouth;
    Map<String, String> trainRecord = new HashMap<>();


    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_screen);
        firebaseAuth= FirebaseAuth.getInstance();

        mLatitudeText = (TextView) findViewById((R.id.latitude_text));
        mLongitudeText = (TextView) findViewById((R.id.longitude_text));
        editTxtTrainNum = (EditText) findViewById(R.id.editTxtTrainNum);
        rBtnNorth = (RadioButton) findViewById(R.id.rBtnNorth);
        rBtnSouth = (RadioButton) findViewById(R.id.rBtnSouth);
        DatabaseReference ref = instantiateFirebase();


        // Builder for Google API Client //
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // Build API client for Location Services API //
                .addApi(LocationServices.API)
                // Add Connection Callback methods to establish and remove location listener //
                .addConnectionCallbacks(this)
                // Add callbacklistener of connection fails //
                .addOnConnectionFailedListener(this)
                .build();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5 * 1000); //15 secs
        mLocationRequest.setFastestInterval(5 * 1000); //5 secs
        // Set to balanced power accuracy on real device //
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        points.add(new LatLng(14.5342787, 120.9983325)); //Baclaran
        points.add(new LatLng(14.6575433, 121.0211903)); //Roosevelt
        //Northbound Arraylist
        northBound.add(new LatLng(14.5342787, 120.9983325)); //Baclaran
        northBound.add(new LatLng(14.538734, 21.0006714));   //EDSA
        northBound.add(new LatLng(14.5476859, 120.9986007)); //Libertad
        northBound.add(new LatLng(14.554166, 120.9971631));  //Gil Puyat
        northBound.add(new LatLng(14.5633769, 120.994792));  //Vito Cruz
        northBound.add(new LatLng(14.5702822, 120.9917343)); //Quirino
        northBound.add(new LatLng(14.5761983, 120.9883949)); //Pedro Gil
        northBound.add(new LatLng(14.582501, 120.9846586));  //U.N.
        northBound.add(new LatLng(14.5927101, 120.9816304)); //Central
        northBound.add(new LatLng(14.5989422, 120.9813166)); //Carriedo
        northBound.add(new LatLng(14.6053066, 120.9820354)); //D. Jose
        northBound.add(new LatLng(14.6112295, 120.982435));  //Bambang
        northBound.add(new LatLng(14.6166306, 120.9826818)); //Tayuman
        northBound.add(new LatLng(14.6226285, 120.9829044)); //Blumentritt
        northBound.add(new LatLng(14.6308815, 120.98122));   //Abad Santos
        northBound.add(new LatLng(14.6360537, 120.9823304)); //R. Papa
        northBound.add(new LatLng(14.6444152, 120.9835804)); //5th Avenue
        northBound.add(new LatLng(14.6543671, 120.9838942)); //Monumento
        northBound.add(new LatLng(14.6574161, 121.0036728)); //Balintawak
        northBound.add(new LatLng(14.6575433, 121.0211903)); //Roosevelt

        //Southbound Arraylist
        southBound.add(new LatLng(14.6575433, 121.0211903)); //Roosevelt
        southBound.add(new LatLng(14.6574161, 121.0036728)); //Balintawak
        southBound.add(new LatLng(14.6543671, 120.9838942)); //Monumento
        southBound.add(new LatLng(14.6444152, 120.9835804)); //5th Avenue
        southBound.add(new LatLng(14.6360537, 120.9823304)); //R. Papa
        southBound.add(new LatLng(14.6308815, 120.98122));   //Abad Santos
        southBound.add(new LatLng(14.6226285, 120.9829044)); //Blumentritt
        southBound.add(new LatLng(14.6166306, 120.9826818)); //Tayuman
        southBound.add(new LatLng(14.6112295, 120.982435));  //Bambang
        southBound.add(new LatLng(14.6053066, 120.9820354)); //D. Jose
        southBound.add(new LatLng(14.5989422, 120.9813166)); //Carriedo
        southBound.add(new LatLng(14.5927101, 120.9816304)); //Central
        southBound.add(new LatLng(14.582501, 120.9846586));  //U.N.
        southBound.add(new LatLng(14.5761983, 120.9883949)); //Pedro Gil
        southBound.add(new LatLng(14.5702822, 120.9917343)); //Quirino
        southBound.add(new LatLng(14.5633769, 120.994792));  //Vito Cruz
        southBound.add(new LatLng(14.554166, 120.9971631));  //Gil Puyat
        southBound.add(new LatLng(14.5476859, 120.9986007)); //Libertad
        southBound.add(new LatLng(14.538734, 21.0006714));   //EDSA
        southBound.add(new LatLng(14.5342787, 120.9983325)); //Baclaran


        rBtnNorth.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (rBtnNorth.isChecked()){
                    rBtnSouth.setChecked(false);
                    Bound = "1";
                }
            }
        });
        rBtnSouth.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if (rBtnSouth.isChecked()){
                    rBtnNorth.setChecked(false);
                    Bound = "0";
                }
            }
        });
    }


    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    private void stopLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection Failed: ConnectionResult.geErrorCode() =" + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        DatabaseReference ref = instantiateFirebase(); //only in pull
        lat = location.getLatitude();
        lng = location.getLongitude();
        LatLng latlng = new LatLng(lat, lng);
        pushLocation.setLatitude(lat);
        pushLocation.setLongitude(lng);
        Latitude = String.valueOf(lat);
        Longitude = String.valueOf(lng);
        oldlat = lat;
        oldlng = lng;

        mLatitudeText.setText(Latitude);
        mLongitudeText.setText(Longitude);

        //Gets or calculates speed of train
        if (location.hasSpeed()){
            spd = location.getSpeed();

        }
        else{
            spd = calculateDistance(lat, lng, oldlat, oldlng) / 5; //Change value depending on interval
        }
        // Sort if train is going northbound or southbound // Roosevelt: 14.6575433, 121.0211903 / Baclaran: 14.5342787, 120.9983325
        distance1 = calculateDistance(lat, lng, 14.6575433, 121.0211903);
        distance2 = calculateDistance(lat, lng, 14.5342787, 120.9983325);

        //Northbound
        if(distance1 > distance2){
            if (stationNum != 20){
                currentStationNorth(stationNum);
                distance = calculateDistance(lat, lng, northBound.get(stationNum).latitude, northBound.get(stationNum).longitude);
                if (distance == 0 && distance <= 50){
                    Toast.makeText(this, "Arrived at Station: " + StationName, Toast.LENGTH_SHORT).show();
                    time = 0;
                    oldtime = 0;
                    stationNum+=1;
                }
            }
            else if (stationNum == 20){
                stationNum = 0;
            }

        }
        //Southbound
        else if (distance2 > distance1){
            if (stationNum != 20){
                currentStationSouth(stationNum);
                distance = calculateDistance(lat, lng, southBound.get(stationNum).latitude, southBound.get(stationNum).longitude);
                if (distance == 0 && distance <= 50){
                    Toast.makeText(this, "Arrived at Station: " + StationName, Toast.LENGTH_SHORT).show();
                    time = 0;
                    oldtime = 0;
                    stationNum+=1;
                }
            }
            else if (stationNum == 20){
                stationNum = 0;
            }
        }

        time = distance / spd;
        convTime = (int) time;
        convOldTime = (int) oldtime;
        timeInMins = convTime / 60;
        oldTimeInMins = convOldTime / 60;

        if (spd == 0){
            oldtime += 5; //subject to change
        } else {
            oldtime = time;
        }

        //Checks if startpush has been clicked
        if (clicked) {
            //String trainNum = editTxtTrainNum.getText().toString();
            // create a HashMap to contain the data to be added to Firebase
            trainRecord.put(LATITUDE, Latitude);
            trainRecord.put(LONGITUDE, Longitude);

            addRecord(ref, trainRecord);


            //Toast.makeText(this, "gotta pushit", Toast.LENGTH_SHORT).show();


        }
        else{
            //Toast.makeText(getActivity(), "Click allow firebase", Toast.LENGTH_SHORT).show();
        }
    }

    public void currentStationNorth(int Stationnum){
        if (northBound.get(Stationnum).latitude == northBound.get(0).latitude && northBound.get(Stationnum).longitude == northBound.get(0).longitude){
            StationName = "Baclaran";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(1).latitude && northBound.get(Stationnum).longitude == northBound.get(1).longitude){
            StationName = "EDSA";
        }
        else if (northBound.get(Stationnum).latitude ==  northBound.get(2).latitude && northBound.get(Stationnum).longitude == northBound.get(2).longitude){
            StationName = "Libertad";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(3).latitude && northBound.get(Stationnum).longitude == northBound.get(3).longitude){
            StationName = "Gil Puyat";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(4).latitude && northBound.get(Stationnum).longitude == northBound.get(4).longitude){
            StationName = "Vito Cruz";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(5).latitude && northBound.get(Stationnum).longitude == northBound.get(5).longitude){
            StationName = "Quirino";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(6).latitude && northBound.get(Stationnum).longitude == northBound.get(6).longitude){
            StationName = "Pedro Gil";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(7).latitude && northBound.get(Stationnum).longitude == northBound.get(7).longitude){
            StationName = "United Nations";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(8).latitude && northBound.get(Stationnum).longitude == northBound.get(8).longitude){
            StationName = "Central";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(9).latitude && northBound.get(Stationnum).longitude == northBound.get(9).longitude){
            StationName = "Carriedo";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(10).latitude && northBound.get(Stationnum).longitude == northBound.get(10).longitude){
            StationName = "Doroteo Jose";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(11).latitude && northBound.get(Stationnum).longitude == northBound.get(11).longitude) {
            StationName = "Bambang";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(12).latitude && northBound.get(Stationnum).longitude == northBound.get(12).longitude) {
            StationName = "Tayuman";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(13).latitude && northBound.get(Stationnum).longitude == northBound.get(13).longitude) {
            StationName = "Blumentritt";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(14).latitude && northBound.get(Stationnum).longitude == northBound.get(14).longitude) {
            StationName = "Abad Santos";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(15).latitude && northBound.get(Stationnum).longitude == northBound.get(15).longitude) {
            StationName = "R. Papa";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(16).latitude && northBound.get(Stationnum).longitude == northBound.get(16).longitude) {
            StationName = "5th Avenue";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(17).latitude && northBound.get(Stationnum).longitude == northBound.get(17).longitude) {
            StationName = "Monumento";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(18).latitude && northBound.get(Stationnum).longitude == northBound.get(18).longitude) {
            StationName = "Balintawak";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(19).latitude && northBound.get(Stationnum).longitude == northBound.get(19).longitude) {
            StationName = "Roosevelt";
        }
    }

    public void currentStationSouth(int Stationnum){
        if (southBound.get(Stationnum).latitude == southBound.get(0).latitude && southBound.get(Stationnum).longitude == southBound.get(0).longitude){
            StationName = "Roosevelt";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(1).latitude && northBound.get(Stationnum).longitude == northBound.get(1).longitude){
            StationName = "Balintawak";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(2).latitude && northBound.get(Stationnum).longitude == northBound.get(2).longitude){
            StationName = "Monumento";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(3).latitude && northBound.get(Stationnum).longitude == northBound.get(3).longitude){
            StationName = "5th Avenue";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(4).latitude && northBound.get(Stationnum).longitude == northBound.get(4).longitude){
            StationName = "R. Papa";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(5).latitude && northBound.get(Stationnum).longitude == northBound.get(5).longitude){
            StationName = "Abad Santos";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(6).latitude && northBound.get(Stationnum).longitude == northBound.get(6).longitude){
            StationName = "Blumentritt";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(7).latitude && northBound.get(Stationnum).longitude == northBound.get(7).longitude){
            StationName = "Tayuman";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(8).latitude && northBound.get(Stationnum).longitude == northBound.get(8).longitude){
            StationName = "Bambang";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(9).latitude && northBound.get(Stationnum).longitude == northBound.get(9).longitude){
            StationName = "D. Jose";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(10).latitude && northBound.get(Stationnum).longitude == northBound.get(10).longitude){
            StationName = "Carriedo";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(11).latitude && northBound.get(Stationnum).longitude == northBound.get(11).longitude){
            StationName = "Central";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(12).latitude && northBound.get(Stationnum).longitude == northBound.get(12).longitude){
            StationName = "United Nations";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(13).latitude && northBound.get(Stationnum).longitude == northBound.get(13).longitude){
            StationName = "Pedro Gil";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(14).latitude && northBound.get(Stationnum).longitude == northBound.get(14).longitude){
            StationName = "Quirino";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(15).latitude && northBound.get(Stationnum).longitude == northBound.get(15).longitude){
            StationName = "Vito Cruz";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(16).latitude && northBound.get(Stationnum).longitude == northBound.get(16).longitude){
            StationName = "Gil Puyat";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(17).latitude && northBound.get(Stationnum).longitude == northBound.get(17).longitude){
            StationName = "Libertad";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(18).latitude && northBound.get(Stationnum).longitude == northBound.get(18).longitude){
            StationName = "EDSA";
        }
        else if (northBound.get(Stationnum).latitude == northBound.get(19).latitude && northBound.get(Stationnum).longitude == northBound.get(19).longitude){
            StationName = "Baclaran";
        }
    }


    // Push location to firebase button
    public void startpush (View v){
        TrainNum = editTxtTrainNum.getText().toString();
        if (TextUtils.isEmpty(TrainNum)){
            editTxtTrainNum.setError("Please enter Train Number");
        }
        if (!rBtnNorth.isChecked() && !rBtnSouth.isChecked()){
            Toast.makeText(this, "Please select if Northbound or Southbound", Toast.LENGTH_SHORT).show();
        }
        if (!(TextUtils.isEmpty(TrainNum)) && rBtnNorth.isChecked()){
            clicked = true;
            trainRecord.put(TRAIN_NUMBER, TrainNum);
            trainRecord.put(BOUND, Bound);
            Toast.makeText(this, "Pushing to database", Toast.LENGTH_SHORT).show();
            editTxtTrainNum.setEnabled(false);
            rBtnNorth.setEnabled(false);
            rBtnSouth.setEnabled(false);
            }
        else if (!(TextUtils.isEmpty(TrainNum)) && rBtnSouth.isChecked()){
            clicked = true;
            trainRecord.put(TRAIN_NUMBER, TrainNum);
            trainRecord.put(BOUND, Bound);
            Toast.makeText(this, "Pushing to database", Toast.LENGTH_SHORT).show();
            editTxtTrainNum.setEnabled(false);
            rBtnNorth.setEnabled(false);
            rBtnSouth.setEnabled(false);
        }
    }

    public void addRecord(DatabaseReference ref, Map map) {
        final String trainNumber = (String) map.get(TRAIN_NUMBER);

        ref.child(trainNumber).setValue(pushLocation, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Toast.makeText(DriverScreen.this, "Success!", Toast.LENGTH_SHORT).show();
            }
        });
        //Adds a new child "Bounds" in trainNumber and sets its value to what is stored in String 'Bound'
        ref.child(trainNumber).child("Bounds").setValue(Bound);
        ref.child(trainNumber).child("Station").setValue(StationName);
        if (spd == 0){
            //push oldtimeinmins to firebase here
            ref.child(trainNumber).child("eta").setValue(oldTimeInMins);
        } else {
            //push timeinmins to firebase here
            ref.child(trainNumber).child("eta").setValue(timeInMins);
        }
    }

    public DatabaseReference instantiateFirebase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Locations");
        return myRef;
    }

    // Formula for Distance is working
    private static double calculateDistance(double lat, double lng, double oldlat, double oldlng) {

        double dLat = Math.toRadians(oldlat - lat);
        double dLng = Math.toRadians(oldlng - lng);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat))
                * Math.cos(Math.toRadians(oldlat)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        //double c = 2 * Math.asin(Math.sqrt(a));
        double c = 2 * Math.atan(Math.sqrt(a));
        long distanceInMeters = Math.round(6371000 * c);
        return distanceInMeters;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

        // This part updates the gps every nth second //
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5 * 1000); //15 secs
        mLocationRequest.setFastestInterval(5 * 1000); //5 secs
        // Set to balanced power accuracy on real device //
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();

        // Check if Google Play Services is installed //
        int response = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (response != ConnectionResult.SUCCESS){
            Log.i(TAG, "Google Play Services is not available - show dialog to ask user to download it");
            GoogleApiAvailability.getInstance().getErrorDialog(this, response, 1).show();
        } else{
            Log.i(TAG, "Google Play Services is available - no action is required");
        }

        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }


    public void stoplogout(View v) {
        if (firebaseAuth.getCurrentUser() != null) {
            DatabaseReference ref = instantiateFirebase();
            if (TrainNum != null) {
                ref.child(TrainNum).removeValue();
            }
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(DriverScreen.this, UserLogin.class));
        }
    }
}



