package com.insomaniacs.adu.trainalert;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ETAFragment extends Fragment {

    List<String> mTrainETARecords = new ArrayList<>();
    ArrayAdapter<String> adapter;

    ListView mTrainsListView;

    RadioButton radBtnNorth;
    RadioButton radBtnSouth;
    public Integer bounds;
    public Integer GetEta;
    public Integer whatBound = 1;

    public ETAFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // create adapter for the ListView

        View v = inflater.inflate(R.layout.fragment_eta, container, false);

        adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, mTrainETARecords);



        DatabaseReference ref = instantiateFirebase("Locations");

        getTrainETA(ref);
        return v;
    }
    @Override
    public void  onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);
//        Button mRetrieveRecords = (Button) getView().findViewById(R.id.retrieveRecordsButton);

//        mRetrieveRecords.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DatabaseReference ref = instantiateFirebase("Reports");
//                getAllRecord(ref);
//            }
//        });
        mTrainsListView = (ListView) getView().findViewById(R.id.trainRecordsListView);
        radBtnNorth = (RadioButton) getView().findViewById(R.id.rBtnAccept);
        radBtnSouth = (RadioButton) getView().findViewById(R.id.rBtnDecline);

        radBtnNorth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radBtnNorth.isChecked()) {
                    radBtnSouth.setChecked(false);
                    mTrainsListView.setEnabled(true);
                    whatBound = 1;
                }
            }
        });
        radBtnSouth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radBtnSouth.isChecked()) {
                    radBtnNorth.setChecked(false);
                    mTrainsListView.setEnabled(true);
                    whatBound = 0;
                }
            }
        });
        //mTrainsListView.setAdapter(adapter);
    }
    public DatabaseReference instantiateFirebase(String parent) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(parent);
        return myRef;
    }

    public void getTrainETA(DatabaseReference ref){
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //OUTPUT IN LISTVIEW / TEXTVIEW everytime a child's data changes
                //Gets train number
                //dataSnapshot.getKey();
                //gets station from Train
                String station = (String) ((Map) dataSnapshot.getValue()).get("Station");
                //gets eta from Train
                Long etaToLong = (Long) ((Map) dataSnapshot.getValue()).get("eta");
                if (etaToLong != null){
                    GetEta = etaToLong.intValue();
                }

                //gets bounds from Train in String and converts to Integer
                String boundsToString = (String) ((Map) dataSnapshot.getValue()).get("Bounds");
                if (boundsToString != null) {
                    bounds = Integer.valueOf(boundsToString);
                }

                //output depends on what radbtn was clicked
                if (whatBound == 1){
                    mTrainETARecords.clear();
                    if (bounds != null) {
                        if (bounds == 1) {
                            mTrainETARecords.add(String.format(Locale.ENGLISH, "Train %s: ETA to %s is %s mins.", dataSnapshot.getKey(), station, GetEta));
                        }
                    }
                }
                else if (whatBound == 0){
                    mTrainETARecords.clear();
                    if (bounds != null) {
                        if (bounds == 0) {
                            mTrainETARecords.add(String.format(Locale.ENGLISH, "Train %s: ETA to %s is %s mins.", dataSnapshot.getKey(), station, GetEta));
                        }
                    }
                }
                mTrainsListView.setAdapter(adapter);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

//    public void getAllRecord(DatabaseReference ref) {
//        final HashMap<String, Object> singleRecord = new HashMap<>();
//        ref.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                // Convert the dataSnapshot (HashMap) object to ArrayList
//                mapToList((Map)dataSnapshot.getValue());
//                // makes the ListView real-time
//                adapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//    }

    //not being used atm
    public void mapToList(Map<String, Object> map) {
        mTrainETARecords.clear();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            // extract HashMap key from Firebase database
            String key = entry.getKey();

            // extract HashMap value from Firebase database
            Object value = entry.getValue();

            // Since the HashMap value is also a HashMap, based on data schema, i.e.
            // "Location" {
            //      [trainNumber] {
            //          "Latitude" : value1,
            //          "Longitude" : value2
            //      }
            // }
            // extract the values from the HashMap trainNumber
//            String reportname = (String) ((Map)(value)).get("ReportName");
//            String trainnumber = (String) ((Map)(value)).get("TrainNumber");
//            String timecreated = (String) ((Map)(value)).get("TimeCreated");
//            String active = (String) ((Map)(value)).get("Active");
//            String yes = (String) ((Map)(value)).get("Yes");
//            String no = (String) ((Map)(value)).get("No");

            // Add the extracted HashMap values to the ArrayList
//            mTrainRecords.add(String.format(Locale.ENGLISH, "%s : %s %s %s %s %s %s",key, reportname, trainnumber, timecreated, active, yes, no));
            mTrainETARecords.add(String.format(Locale.ENGLISH, "%s : %s", key, value));
        }
    }
}
