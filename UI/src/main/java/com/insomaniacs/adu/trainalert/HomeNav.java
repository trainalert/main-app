package com.insomaniacs.adu.trainalert;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class HomeNav extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_nav);

        TextView name, email;
        firebaseAuth = FirebaseAuth.getInstance();



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        TrainMapFragment trainmap= new TrainMapFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment,trainmap).commit();


        if (firebaseAuth.getCurrentUser()!=null) {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            View header= navigationView.getHeaderView(0);
//            name = (TextView)header.findViewById(R.id.UserName);
            email = (TextView)header.findViewById(R.id.UserEmail);

//          name.setText(user.getDisplayName());
            email.setText(user.getEmail());
        }

//        if (firebaseAuth.getCurrentUser() ==null){
//            //            finish();
//            startActivity(new Intent(this,HomeNav.class));
//        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//           super.onBackPressed();
           moveTaskToBack(true);
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_nav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_map) {
            setTitle("Train Map");
            TrainMapFragment trainmap= new TrainMapFragment();
            FragmentManager fragmentManager =getSupportFragmentManager();
          fragmentManager.beginTransaction().replace(R.id.fragment,trainmap).commit();
        }

        if (id == R.id.nav_ETA) {
            setTitle("ETA of trains");
            ETAFragment eta= new ETAFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,eta).commit();
        }
        else if (id == R.id.nav_reportlog) {

            if (firebaseAuth.getCurrentUser() == null) {
                Intent login = new Intent(HomeNav.this, UserLogin.class);
                startActivity(login);
            } else {
                Intent report = new Intent(HomeNav.this, SendReport.class);
                startActivity(report);

            }
        }
//        }
//        else if (id == R.id.nav_comment) {
//            setTitle("Comment");
//            CommentFragment comment= new CommentFragment();
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.fragment,comment).commit();
//        }
         else if (id == R.id.nav_viewreports) {
            setTitle("View Reports");
            ViewReports viewreports= new ViewReports();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,viewreports).commit();

        } else if (id == R.id.nav_reportfragment){
            setTitle("Verified Reports");
            ReportFragment reportfragment = new ReportFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment,reportfragment).commit();
        }

        else if (id == R.id.nav_logout) {
//            if (firebaseAuth.getCurrentUser() !=null) {
//                firebaseAuth.signOut();
//                finish();
//                startActivity(new Intent(HomeNav.this, HomeNav.class));
//            }
//            else{
//                startActivity(new Intent(HomeNav.this, HomeNav.class));
//            }
            firebaseAuth.signOut();
                finish();
                startActivity(new Intent(HomeNav.this, UserLogin.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
}
