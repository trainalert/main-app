package com.insomaniacs.adu.trainalert;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AdminScreen extends AppCompatActivity {

    List<String> mAdminRecords = new ArrayList<>();
    List<String> mVerifyRecords = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapter2;

    ListView mAdminListView;
    ListView mVerifyListView;

    final String UID = "UID";
    final String REPORT_NAME = "REPORT_NAME";
    final String TRAIN_NUMBER = "TRAIN_NUMBER";

    String reportName;
    String trainNumber;
    String UIDofReporter;
    String reportName2;
    String trainNumber2;
    String UIDofReporter2;
    String storeUID;
    String storedUID;

    RadioButton radBtnAccept;
    RadioButton radBtnDecline;
    RadioButton radBtnDelete;


    public void onBackPressed() {
        moveTaskToBack(true);
//        startActivity(new Intent(AdminScreen.this, HomeNav.class));
    }



    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_screen);
        firebaseAuth= FirebaseAuth.getInstance();

        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mAdminRecords);
        adapter2 = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mVerifyRecords);

        final DatabaseReference ref = instantiateFirebase("AdminReport");
        final DatabaseReference del = instantiateFirebase("VerifiedReport");

        mAdminListView = (ListView) findViewById(R.id.adminRecordsListView);
        mVerifyListView = (ListView) findViewById(R.id.verifyRecordsListView);

        radBtnAccept = (RadioButton) findViewById(R.id.rBtnAccept);
        radBtnDecline = (RadioButton) findViewById(R.id.rBtnDecline);
        radBtnDelete = (RadioButton) findViewById(R.id.rBtnDelete);



        getAdminReports(ref);

        radBtnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radBtnAccept.isChecked()) {
                    radBtnDecline.setChecked(false);
                    mAdminListView.setEnabled(true);


                }
            }
        });
        radBtnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radBtnDecline.isChecked()) {
                    radBtnAccept.setChecked(false);
                    mAdminListView.setEnabled(true);


                }
            }
        });
        mAdminListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (radBtnAccept.isChecked()) {
                            String hisUID = (String) mAdminListView.getItemAtPosition(position).toString();
                            //Separates the string to only get the UID
                            String userID = hisUID.substring(0, hisUID.indexOf(" "));

                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                String compareme = postSnapshot.getKey();
                                if (compareme.trim().equals(userID.trim())) {
                                    storeUID = postSnapshot.getKey();
                                    //put codes to push to firebase here
                                    trainNumber = (String) ((Map) postSnapshot.getValue()).get("TrainNumber");
                                    reportName = (String) ((Map) postSnapshot.getValue()).get("ReportName");
                                    DatabaseReference verify = instantiateFirebase("VerifiedReport");
                                    Map<String, String> verifyRecord = new HashMap<>();
                                    verifyRecord.put(UID, storeUID);
                                    verifyRecord.put(REPORT_NAME, reportName);
                                    verifyRecord.put(TRAIN_NUMBER, trainNumber);
                                    addVerify(verify, verifyRecord);
                                    ref.child(storeUID).removeValue();
                                    radBtnAccept.setChecked(false);
                                    adapter.notifyDataSetChanged();
                                }
                            }


                        }
                        else if (radBtnDecline.isChecked()){
                            String hisUID = (String) mAdminListView.getItemAtPosition(position).toString();
                            //Separates the string to only get the UID
                            String userID = hisUID.substring(0, hisUID.indexOf(" "));

                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                String compareme = postSnapshot.getKey();
                                if (compareme.trim().equals(userID.trim())) {
                                    storeUID = postSnapshot.getKey();
                                    ref.child(storeUID).removeValue();
                                    radBtnDecline.setChecked(false);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }

                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        });

        mAdminListView.setAdapter(adapter);

        getVerifyReports(del);

        radBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radBtnDelete.isChecked()) {
                    radBtnAccept.setChecked(false);
                    radBtnDecline.setChecked(false);
                    mVerifyListView.setEnabled(true);


                }
            }
        });

        mVerifyListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
                del.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (radBtnDelete.isChecked()) {
                            String hisUID = (String) mVerifyListView.getItemAtPosition(position).toString();
                            //Separates the string to only get the UID
                            String userID = hisUID.substring(0, hisUID.indexOf(" "));

                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                String compareme = postSnapshot.getKey();
                                if (compareme.trim().equals(userID.trim())) {
                                    storedUID = postSnapshot.getKey();
                                    del.child(storedUID).removeValue();
                                    radBtnDelete.setChecked(false);
                                    adapter2.notifyDataSetChanged();
                                }
                            }
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        });

        mVerifyListView.setAdapter(adapter2);
    }

    public DatabaseReference instantiateFirebase(String parent) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(parent);
        return myRef;
    }

    public void getAdminReports(DatabaseReference ref){
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mAdminRecords.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    UIDofReporter = (String) postSnapshot.getKey();
                    trainNumber = (String) ((Map) postSnapshot.getValue()).get("TrainNumber");
                    reportName = (String) ((Map) postSnapshot.getValue()).get("ReportName");

                    mAdminRecords.add(String.format(Locale.ENGLISH, "%s : Train %s: %s", UIDofReporter ,trainNumber, reportName));
                }

                /*
                trainNumber = dataSnapshot.child(uid).child("TrainNumber").getValue().toString();
                reportName = dataSnapshot.child(uid).child("ReportName").getValue().toString();

                mAdminRecords.add(String.format(Locale.ENGLISH, "Train %s: %s", trainNumber, reportName));

                mAdminListView.setAdapter(adapter);
                */
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void getVerifyReports(DatabaseReference del){
        del.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mVerifyRecords.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    UIDofReporter2 = (String) postSnapshot.getKey();
                    trainNumber2 = (String) ((Map) postSnapshot.getValue()).get("TrainNumber");
                    reportName2 = (String) ((Map) postSnapshot.getValue()).get("ReportName");

                    mVerifyRecords.add(String.format(Locale.ENGLISH, "%s : Train %s: %s", UIDofReporter2 ,trainNumber2, reportName2));
                }

                /*
                trainNumber = dataSnapshot.child(uid).child("TrainNumber").getValue().toString();
                reportName = dataSnapshot.child(uid).child("ReportName").getValue().toString();

                mAdminRecords.add(String.format(Locale.ENGLISH, "Train %s: %s", trainNumber, reportName));

                mAdminListView.setAdapter(adapter);
                */
                adapter2.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public static class Verify {
        public String ReportName;
        public String TrainNumber;


        public Verify (String reportname, String trainnumber) {
            this.ReportName = reportname;
            this.TrainNumber = trainnumber;
        }
    }
    public void addVerify(DatabaseReference ref, Map map) {
        final String uid = (String) map.get(UID);
        final String reportName = (String) map.get(REPORT_NAME);
        final String trainNumber = (String) map.get(TRAIN_NUMBER);

        Verify verify = new Verify(reportName ,trainNumber);

        ref.child(uid).setValue(verify, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                Toast.makeText(AdminScreen.this, "Success!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void logout(View v) {
        if (firebaseAuth.getCurrentUser() != null) {
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(AdminScreen.this, UserLogin.class));
        }
    }
}



