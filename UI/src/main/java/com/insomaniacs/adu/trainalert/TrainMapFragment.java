package com.insomaniacs.adu.trainalert;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
//import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.maps.android.data.kml.KmlLayer;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class TrainMapFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    //SupportMapFragment mapFragment;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    private Location mLocation;
    double lat, lng, distance, spd;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    protected final static String TAG = "MapsActivity";
    //Marker train;
    private DatabaseReference trainLocationDatabase;
    // This area is used for firebase //
    final String TRAIN_NUMBER = "TRAIN_NUMBER";
    final String LATITUDE = "LATITUDE";
    final String LONGITUDE = "LONGITUDE";
    public Integer bounds;
    //String TrainNumber = "2";
    String Latitude;
    String Longitude;
    //boolean clicked = false;
    public String whatBound;
    DatabaseReference ref = instantiateFirebase();
    //ArrayList<Marker> markersToClear = new ArrayList<Marker>();
    int i = 1, trainSize;
    HashMap<Integer, Marker> mHashMap = new HashMap<Integer, Marker>();
    int stationNum = 0; // used to check if train has passed all stations
    List<String> mTrainRecords = new ArrayList<>();
    int x = 1;
    Map<String, Marker> markers = new HashMap();
    String eta;

    public TrainMapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);





    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_train_map, container, false);

        //Might need to add this in onCreateView
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                // Build API client for Location Services API //
                .addApi(LocationServices.API)
                // Add Connection Callback methods to establish and remove location listener //
                .addConnectionCallbacks(this)
                // Add callbacklistener of connection fails //
                .addOnConnectionFailedListener(this)
                .build();

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        fragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng start = new LatLng(14.5342787, 120.9983325);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(start, 15));

        try {
            KmlLayer kmlLayer = new KmlLayer(mMap, R.raw.lrt1map, getActivity().getApplicationContext());
            kmlLayer.addLayerToMap();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            // This shows the GPS area of the phone //
            mMap.setMyLocationEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        // This shows the GPS area of the phone //
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(getActivity(), "This app requires location permissions to be granted", Toast.LENGTH_LONG).show();
                    getActivity().finish();
                }
                break;
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    private void stopLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection Failed: ConnectionResult.geErrorCode() =" + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        DatabaseReference ref = instantiateFirebase(); //only in pull
        lat = location.getLatitude();
        lng = location.getLongitude();
        LatLng latlng = new LatLng(lat, lng);


        getTrains(ref);
        //getRecord(ref);
        //getAllRecord(ref);


    }

    public void getTrains(DatabaseReference ref){
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Double latitude = (Double) ((Map) dataSnapshot.getValue()).get("latitude");
                Double longitude = (Double) ((Map) dataSnapshot.getValue()).get("longitude");
                Long etaToLong = (Long) ((Map) dataSnapshot.getValue()).get("eta");
                if (etaToLong != null) {
                    eta = String.valueOf(etaToLong);
                }
                //To be used in 'if else' to determine what to output in marker if north or southbound 1=north / 0=south
                String boundsToString = (String) ((Map) dataSnapshot.getValue()).get("Bounds");
                if (boundsToString != null) {
                     bounds = Integer.valueOf(boundsToString);
                }
                //To be used to output in marker what station
                String station = (String) ((Map) dataSnapshot.getValue()).get("Station");
                LatLng pulled = new LatLng(latitude, longitude);

                if (markers.containsKey(dataSnapshot.getKey())) {
                    //Toast.makeText(getActivity(), "Keys: " + markers, Toast.LENGTH_SHORT).show();
                    Marker marker = markers.get(dataSnapshot.getKey());

                    marker.remove();
                    markers.get(dataSnapshot.getKey()).remove();
                    markers.remove(dataSnapshot.getKey());
                    // or
                    // marker.setPosition(newPosition);
                }
                //Toast.makeText(getActivity(), "bounds: " + bounds, Toast.LENGTH_SHORT).show();
                Marker train;
                //put if northbound or southbound here
                if (bounds != null){
                    if (bounds == 1) {
                        whatBound = "Northbound";
                            train = mMap.addMarker(new MarkerOptions().position(pulled).title("Train " + dataSnapshot.getKey() + " " + whatBound + " ETA to " + station + ": " + eta + " mins."));
                            markers.put(dataSnapshot.getKey(), train);

                            //Adds a new child "eta" in trainNumber and sets its value to what is stored in String 'Bound'
                            //instantiateFirebase().child(dataSnapshot.getKey()).child("eta").setValue(oldTimeInMins);
                    }
                    else if (bounds == 0){
                        whatBound = "Southbound";
                            train = mMap.addMarker(new MarkerOptions().position(pulled).title("Train " + dataSnapshot.getKey() + " " + whatBound + " ETA to " + station + ": " + eta + " mins."));
                            markers.put(dataSnapshot.getKey(), train);
                    }
                        //instantiateFirebase().child(dataSnapshot.getKey()).child("eta").setValue(timeInMins);
                }

                //output is 35791394 for some odd reason, does not increment
                //instantiateFirebase().child(dataSnapshot.getKey()).child("eta").setValue(timeInMins);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if (markers.containsKey(dataSnapshot.getKey())) {
                    Marker marker = markers.get(dataSnapshot.getKey());
                    marker.remove();
                }
                //markers.get(dataSnapshot.getKey()).remove();
                //markers.remove(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public DatabaseReference instantiateFirebase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("Locations");
        return myRef;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

        // This part updates the gps every nth second //
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5 * 1000); //15 secs
        mLocationRequest.setFastestInterval(5 * 1000); //5 secs
        // Set to balanced power accuracy on real device //
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

}
