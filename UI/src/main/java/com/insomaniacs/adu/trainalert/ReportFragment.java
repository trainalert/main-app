package com.insomaniacs.adu.trainalert;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportFragment extends Fragment {
    ArrayAdapter<String> adapter;
    List<String> mVerifiedRecords = new ArrayList<>();
    ListView mVerifiedRecordsListView;


    public ReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            // create adapter for the ListView

            View v = inflater.inflate(R.layout.fragment_report, container, false);

            adapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_list_item_1, android.R.id.text1, mVerifiedRecords);


            //WALA PA SA FIREBASE
            DatabaseReference ref = instantiateFirebase("VerifiedReport");

            getVerifiedRecords(ref);
            return v;
    }
    @Override
    public void  onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);
//        Button mRetrieveRecords = (Button) getView().findViewById(R.id.retrieveRecordsButton);

//        mRetrieveRecords.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DatabaseReference ref = instantiateFirebase("Reports");
//                getAllRecord(ref);
//            }
//        });
        mVerifiedRecordsListView = (ListView) getView().findViewById(R.id.VerifiedRecordsListView);

    }

    public DatabaseReference instantiateFirebase(String parent) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(parent);
        return myRef;
    }

    public void getVerifiedRecords(DatabaseReference ref) {
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mVerifiedRecords.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String trainNumber = (String) ((Map) postSnapshot.getValue()).get("TrainNumber");
                    String reportName = (String) ((Map) postSnapshot.getValue()).get("ReportName");

                    mVerifiedRecords.add(String.format(Locale.ENGLISH, "Train %s: %s", trainNumber, reportName));
                }

                mVerifiedRecordsListView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
