package com.insomaniacs.adu.trainalert;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.data.kml.KmlLayer;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    //SupportMapFragment mapFragment;
    Button markBtn;
    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    private Location mLocation;
    double lat, lng;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    ZoomControls zoom;
    protected final static String TAG = "MapsActivity";
    Marker train;
    //private ArrayList<Marker> mMarkerArrayList;
    //private Marker marker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Builder for Google API Client //
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // Build API client for Location Services API //
                .addApi(LocationServices.API)
                // Add Connection Callback methods to establish and remove location listener //
                .addConnectionCallbacks(this)
                // Add callbacklistener of connection fails //
                .addOnConnectionFailedListener(this)
                .build();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(15 * 1000); //15 secs
        mLocationRequest.setFastestInterval(5 * 1000); //5 secs
        // Set to balanced power accuracy on real device //
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Adds Zoom in and Zoom out buttons //
        zoom = (ZoomControls) findViewById(R.id.zcZoom);
        zoom.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomOut());
            }
        });
        zoom.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
            }
        });

        markBtn = (Button) findViewById(R.id.btnMark);
        markBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                LatLng latlng = new LatLng(lat, lng);
                mMap.addMarker(new MarkerOptions().position(latlng).title("Train 1: " + latlng));
                // Moves and zooms in the camera to where the coordinates are //
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 11));
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // When map is ready, will move the camera to philippines LRT 1 station, zoomed in //
        LatLng start = new LatLng(14.5342787, 120.9983325);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(start, 11));

        try {
            KmlLayer kmlLayer = new KmlLayer(mMap, R.raw.lrt1map, getApplicationContext());
            kmlLayer.addLayerToMap();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }


        // Add a marker of your location and move the camera (needs gps turned on) //
        // Users of this marker are the Train drivers //
        // Creates a latitude and longitude //
        //LatLng latlng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        // mMarkerArrayList = new ArrayList<>();
        //***there should be if login add a marker here
        // marker = mMap.addMarker(new MarkerOptions().position(latLng).title("Train 1"));

        // Adds a marker to where the coordinate are //
        //Marker train = mMap.addMarker(new MarkerOptions().position(latLng).title("Train 1"));
        // Removes the marker //
        //train.remove();

        //mMap.addMarker(new MarkerOptions().position(latlng).title("Train 1: " + latlng));
        // Moves and zooms in the camera to where the coordinates are //
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 4));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            mMap.setMyLocationEnabled(true);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSION_FINE_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "This app requires location permissions to be granted", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    private void stopLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
                startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection Failed: ConnectionResult.geErrorCode() =" + connectionResult.getErrorCode());
    }

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();
        LatLng latlng = new LatLng(lat, lng);
        //mMap.addMarker(new MarkerOptions().position(latlng).title("Train 1: " + latlng));
        // Moves and zooms in the camera to where the coordinates are //
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 11));
        if (train != null){
            train.remove();
        }
        train = mMap.addMarker(new MarkerOptions().position(latlng).title("Train 1"));

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }
}
