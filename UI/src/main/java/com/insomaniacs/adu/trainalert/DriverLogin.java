package com.insomaniacs.adu.trainalert;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class DriverLogin extends AppCompatActivity {

    public void onBackPressed() {
//        moveTaskToBack(true);
        finish();
        Intent back = new Intent(DriverLogin.this, UserLogin.class);
        startActivity(back);
    }

    private Button loginbutton;
    private EditText  editTextEmail;
    private EditText editTextPassword;
    private ProgressDialog progressdialog;
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_login);

        editTextEmail= (EditText) findViewById(R.id.EmailEditText);
        editTextPassword= (EditText) findViewById(R.id.PasswordEditText);
        loginbutton= (Button) findViewById(R.id.SignInButton);
        firebaseAuth= FirebaseAuth.getInstance();

       if (firebaseAuth.getCurrentUser() !=null){
            finish();
            startActivity(new Intent(getApplicationContext(), UserLogin.class));
        }
   }

    private void DriverLogin(){
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter Email address", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }

        progressdialog = ProgressDialog.show(DriverLogin.this, "", "Authenticating...");
        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressdialog.dismiss();
                if(task.isSuccessful()){
                    finish();
                    startActivity(new Intent(getApplicationContext(), DriverScreen.class));
                }
                else{
                    Toast.makeText(getBaseContext(), "Email address is not registered", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
    }

    public void LoginDriverClick(View v){
                 DriverLogin();
    }

    public void SignupDriverClick (View v){
        if (firebaseAuth.getCurrentUser() !=null) {
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(DriverLogin.this, DriverSignup.class));
        }
        else{
            finish();
            startActivity(new Intent(DriverLogin.this, DriverSignup.class));
        }
    }
}

