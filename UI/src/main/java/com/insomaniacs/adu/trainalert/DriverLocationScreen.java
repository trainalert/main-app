package com.insomaniacs.adu.trainalert;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;

public class DriverLocationScreen extends AppCompatActivity {
    public void onBackPressed() {
        moveTaskToBack(true);
//        startActivity(new Intent(AdminScreen.this, HomeNav.class));
    }

    Spinner spinner;
    String stations[] = {"Baclaran", "EDSA", "Libertad", "Gil Puyat", "Vito Cruz", "Qurino", "Pedro Gil", "UN Avenue", "Central", "Carriedo", "Doroteo Jose", "Bambang", "Tayuman", "Blumentritt", "Abad Santos", "R.Papa", "5th Avenue", "Monumento", "Balintawak", "Roosevelt"};
    ArrayAdapter<String> adapter;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driverloc_screen);

        spinner = (Spinner) findViewById(R.id.spinner);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, stations);
        spinner.setEnabled(false);
        spinner.setClickable(false);
        spinner.setAdapter(adapter);
    }


    public void NorthClick(View v) {
        RadioButton south=(RadioButton) findViewById(R.id.southradio);
        south.setChecked(false);

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setEnabled(true);
        spinner.setClickable(true);
    }

    public void SouthClick(View v) {
        RadioButton north=(RadioButton) findViewById(R.id.northradio);
        north.setChecked(false);

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setEnabled(true);
        spinner.setClickable(true);
    }

    public void ProceedClick(View v) {
        startActivity(new Intent(DriverLocationScreen.this, DriverScreen.class));
    }
}





